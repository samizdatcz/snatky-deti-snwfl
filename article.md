title: "Kalkulačka: Spočítejte si, kdy vás čeká svatba, dítě, rozvod, nebo úmrtí"
perex: "Pravděpodobnost, že vás letos čeká velká událost, lze spočítat z populačních statistik. "
authors: ["Jan Boček"]
coverimg: https://www.irozhlas.cz/sites/default/files/uploader/svatby_170420-033802_cib.jpg
coverimg_note: "Foto <a href='https://www.flickr.com/photos/pitel/8474243116/'>Jan Kaláb</a> (CC BY-SA 2.0)"
styles: []
libraries: ["https://interaktivni.rozhlas.cz/tools/d3/3.5.3.min.js", "https://interaktivni.rozhlas.cz/tools/jquery/2.1.1.min.js", "https://samizdat.cz/data/snatky-deti-rozvody/www/js/_v1.1.5.d3.js"]
---

<p id=fallback>Váš prohlížeč je bohužel zastaralý a nepodporuje interaktivní vizualizace. Doporučujeme jej aktualizovat nebo <a target=_blank href="http://www.browserchoice.eu/">stáhnout jiný</a></p>

<div id="container">
  <div id="otazka">
    <label>Kolik vám je let?</label>
    <input id="vek" type="number" min=0 max=109 value=18 />
  </div>
  <div id="otazka">
    <label>Jste žena nebo muž?</label>
    <select id="pohlavi">
      <option value="zena">žena</option>
      <option value="muz">muž</option>
    </select>
  </div>
  <div>
    <button id="ja">Jak jsem na tom?</button>
    <button id="ostatni">Jak jsem na tom v porovnání s ostatními?</button>
    <button id="minulost">Jak bych na tom byl(a) v minulosti?</button>
  </div>
  <div id="result"></div>
</div>

Typický věk, kdy si ženy pořizují první dítě, se od padesátých let minulého století příliš neměnil. Téměř půlstoletí se pohyboval kolem 23 let.

V polovině devadesátých let se ovšem tradiční systém začal rozpadat. Dramaticky rychle – během pouhých patnácti let mezi roky 1995 a 2010 – se průměrný věk prvorodičky zvedl o pět let. Dnes je nad hranicí 28 let.

„Zvyšování průměrného věku matky při porodu je součástí takzvaného druhého demografického přechodu, který v zemích západní a severní Evropy nastal už v šedesátých letech minulého století a trval přibližně dvacet let,“ vysvětluje Eva Kačerová z Českého statistického úřadu. „Tehdy došlo k proměně hodnotové orientace, která se projevila růstem individualismu a odrazila se i v demografickém chování. Rodina ztrácela svou nezastupitelnou funkci, narůstal počet nesezdaných párů a neúplných rodin. Zvyšoval se věk vstupu do manželství a věk, kdy lidé přivádějí na svět první dítě.“

Stejné trendy se v osmdesátých letech začaly projevovat v komunistickém Československu. Železná opona ovšem řadu demografických změn odrazila nebo alespoň zpomalila, takže se v plné síle projevily až v samostatném porevolučním Česku.

Věk prvorodiček naznačuje, že nejprudší demografickou změnu už může mít česká společnost za sebou: po divokém přelomu století se v posledních několika letech na hranici zmíněných 28 let ustálil.

## Nejdéle se děti odkládají v Praze

Zvlášť viditelné jsou nedávné demografické změny ve velkých městech. Nejdéle se děti odkládají v Praze, kde se oproti celorepublikovému průměru rodí ještě asi o rok a půl později. Mírně starší jsou také rodičky ve Zlínském, Jihomoravském a Středočeském kraji.

Naopak o rok a půl mladší, než je celorepublikový průměr, jsou rodičky na Ústecku, následované Karlovarským a Moravskoslezským krajem.

„“

Pohled na průměrný věk všech rodiček – tedy nejen prvního, ale i ostatních dětí – může působit překvapivě. Je totiž na desetinu stejný jako v roce 1920, 30,2 roku. Důvod je jednoduchý: dříve sice žena obvykle první dítě povila dříve, zároveň ale měla dětí více, rodila tedy do vyššího věku.

„Za první republiky byla úhrnná plodnost téměř tři děti na jednu ženu,“ doplňuje Kačerová. „V roce 2012 je méně než poloviční.“

## Dvě třetiny rozvodů navrhuje žena

Vedle věku, kdy si ženy pořizují první dítě, zasáhly demografické změny také věk prvního sňatku. Do začátku devadesátých let se ženy poprvé vdávaly ve 22, muži poprvé ženili ve 24 letech. Dnes je to 29, respektive 32 let.

Vyšší věk znamená častější páry s vysokým věkovým rozdílem, ať už je výrazně starší muž, nebo žena. To může mít podle Ivy Kohoutové z ČSÚ vliv na stabilitu svazku.

„Výrazné rozdíly ve věku manželů znamenají větší riziko rozvodu,“ popisuje. „Mírně nižší stabilitu také vykazují manželské páry, ve kterých je muž mladší než žena.“

Rozvodovost je další charakteristika, která ilustruje demografické změny. Na rozdíl od věku rodičky a věku sňatku, které se výrazněji změnily až v devadesátých letech, se ovšem rozvodovost mění už delší dobu: v polovině padesátých let končila rozvodem desetina manželství, v roce 1975 se rozvodovost přehoupla přes třicet procent a na padesát procent vystoupala v roce 2010.

Evidence rozvodů nabízí poměrně detailní pohled: téměř dvě třetiny rozvodů v současnosti navrhuje žena, v polovině případů se rodiče starají o nezletilé dítě nebo děti. Nejrizikovější je pátý a šestý rok manželství. Průměrný věk rozvedeného muže i ženy je těsně po čtyřicítce.

Suverénně nejčastější příčinou rozvodu je dnes „rozdíl povah, názorů a zájmů“, uvádají ho tři čtvrtiny rozváděných párů. Detailnější příčiny může odhalit sonda do poloviny osmdesátých let, kdy byly páry sdílnější. Tehdy byla vedle rozdílnosti povah druhou nejčastější příčinou nevěra (jako důvod rozvodu ji udávalo 16 procent žen i mužů). Ženám na mužích často vadil alkoholismus (13 procent rozvodů), nezájem o rodinu (8 procent) a „zlé nakládání“ (5 procent). Sexuální neshody stály za třemi procenty rozvodů.

## Seniorů je víc než dětí

Poslední klíčovou demografickou změnou je vyšší délka dožití. Jak ukazuje kalkulačka, i tady se to podstatné – hlavně u mužů – odehrálo až po roce 1989. Nejdůležitějším impulsem bylo zlepšení péče o pacienty s nemocemi srdce a krevního oběhu na konci osmdesátých let.

Lepší životní podmínky a lékařská péče znamenají, že česká populace rychle stárne. V současnosti je více než 16 procent Čechů starších 65 let a podle prognózy ČSÚ se jejich podíl má během devadesáti let zdvojnásobit. V roce 2006 bylo v Česku poprvé více seniorů než dětí.

*Vizualizace ukazuje příčiny všech úmrtí v českých zemích od roku 1918 do 2006*

<iframe src="https://datasklad.ihned.cz/umrti-2/www/" width="100%" height="600" scrolling="no" frameborder="0"></iframe>